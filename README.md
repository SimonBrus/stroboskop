# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git pull origin master
```

Naloga 6.2.3:
https://bitbucket.org/SimonBrus/stroboskop/commits/7d68646a7119cb834dae035d12afabbd0df64621

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/SimonBrus/stroboskop/commits/89e597c0c5cba371c26685ec7f23a727c52bd93d

Naloga 6.3.2:
https://bitbucket.org/SimonBrus/stroboskop/commits/fdfe0cbeb79e7f7d004448cbeab8e3ac4951627e

Naloga 6.3.3:
https://bitbucket.org/SimonBrus/stroboskop/commits/8ea975e9258c8891d5fdfba8550e9d026744b19c

Naloga 6.3.4:
https://bitbucket.org/SimonBrus/stroboskop/commits/51994def7a800d93fa42eb017e55d848d173d497

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/SimonBrus/stroboskop/commits/fe4e59b77954f3de9e9fed63266cfe0a53d129e3

Naloga 6.4.2:
https://bitbucket.org/SimonBrus/stroboskop/commits/94506bab8001d2440e5ebaf1721ab6a0e7af4835

Naloga 6.4.3:
https://bitbucket.org/SimonBrus/stroboskop/commits/94506bab8001d2440e5ebaf1721ab6a0e7af4835

Naloga 6.4.4:
https://bitbucket.org/SimonBrus/stroboskop/commits/584c12b20e85d179572782c172ffddaf976586af